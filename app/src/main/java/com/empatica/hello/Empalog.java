package com.empatica.hello;

import android.content.Context;

import com.empatica.empaticalogger.EmpaticaLogger;

import java.io.IOException;

/**
 * Created by felipe on 21/07/2016.
 */
public class Empalog {

    private static EmpaticaLogger mEmpaLog;

    private static Empalog ourInstance;

    public static Empalog getInstance(Context ctx) {
        if(ourInstance==null) {
            ourInstance = new Empalog();

            try {
                mEmpaLog = EmpaticaLogger.createInstance(ctx, "7d5fa2ab-631e-443e-88dc-52fe22988142");
            } catch (IOException e) {
                e.printStackTrace();
            }

            mEmpaLog.configureLog(true, EmpaticaLogger.DEBUG, true, EmpaticaLogger.DEBUG);
        }
        return ourInstance;
    }

    private Empalog() {
    }

    public void d(String message) {
        mEmpaLog.log(EmpaticaLogger.DEBUG,message);
    }

    public void w(String message) {
        mEmpaLog.log(EmpaticaLogger.WARNING,message);
    }

    public void i(String message) {
        mEmpaLog.log(EmpaticaLogger.INFO,message);
    }

    public void e(String message) {
        mEmpaLog.log(EmpaticaLogger.ERROR,message);
    }

}
