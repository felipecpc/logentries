package com.empatica.hello;

import android.app.Activity;
import android.os.Bundle;


public class MainActivity extends Activity {

    private Empalog log;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        log = Empalog.getInstance(this);

        log.d("debug MyActivity has been created");
        log.i("info MyActivity has been created");
        log.w("warning MyActivity has been created");
        log.e("error Opa");

    }



}
